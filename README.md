## Software pro sběr a vyhodnocení dat z bezpečnostních her (VI20202022158-R2)

Software poskytuje interaktivní nástroje, které poskytují hráčům a lektorům rychlý vhled do dějů při hraní bezpečnostních her. Chování hráčů je automatizovaně monitorováno v průběhu hry a následně vizualizováno. Prezentované informace umožňují lektorům a tvůrcům her efektivní analýzu průběhu her po jejich skončení, ale i efektivní řízení výuky v průběhu hraní hry. Sada explorativních vizualizací je vytvořena jako samostatné moduly, které jsou plně integrovány do uživatelského rozhraní Software pro přípravu a realizaci bezpečnostních her, což umožňuje jejich rozšiřitelnost a znovupoužitelnost. Přitom zůstávají snadno dostupné a použitelné pro koncové uživatele.

### Obsah repozitářů

Tento repozitář slouží jako rozcestník pro služby a komponenty tohoto software. Sada explorativních vizualizací je vytvořena formou samostatných modulů (viz část *Angular frontend*), které jsou plně integrovány do uživatelského rozhraní [Software pro přípravu a realizaci bezpečnostních her](https://gitlab.ics.muni.cz/muni-kypo-crp/kypolab-software/sw1). Pro kompletní spuštění tohoto software jsou vyžadovány rovněž backendové služby, které se nacházejí v sekci *Java Backend*.

Níže je seznam projektů backendové a frontendové části software. Každá položka obsahuje podrobnější instrukce k sestavení a spuštení.

### Java Backend

- [KYPO Training](https://gitlab.ics.muni.cz/muni-kypo-crp/backend-java/kypo-training)
- [KYPO Training Feedback](https://gitlab.ics.muni.cz/muni-kypo-crp/backend-java/kypo-training-feedback)
- [KYPO User and Group](https://gitlab.ics.muni.cz/muni-kypo-crp/backend-java/kypo-user-and-group)
- [KYPO Elasticsearch Service](https://gitlab.ics.muni.cz/muni-kypo-crp/backend-java/kypo-elasticsearch-service)

### Angular Frontend

- [KYPO Training Agenda](https://gitlab.ics.muni.cz/muni-kypo-crp/frontend-angular/agendas/kypo-training-agenda)
- [Components](https://gitlab.ics.muni.cz/muni-kypo-crp/frontend-angular/components) (tato skupina obsahuje následující vizualizační komponenty frontendové části)
   - Visualization Dashboard  
   - Statistical Visualizations 
   - Clustering Visualization 
   - Overview Visualization 
   - Walkthrough Visualization 
   - Hurdling Visualization 
   - Command Visualizations 
   - Assessment Visualization 
   - Adaptive Transition Visualization 
   - Adaptive Visualization 


### Poděkování

<table>
  <tr>
    <td>![MV ČR](logo_mvcr.png "Logo MV ČR")</td>
    <td>
Tento software je výsledkem projektu Výzkum nových technologií pro zvýšení schopností odborníků na kyberbezpečnost – VI20202022158, který byl podpořen Ministerstvem vnitra ČR z Programu bezpečnostního výzkumu České republiky 2015-2022 (BV III/1-VS).
</td>
  </tr>
</table>
